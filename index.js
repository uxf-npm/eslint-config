module.exports = {
    extends: [
        "@uxf/eslint-config/light",
        "plugin:react/recommended", // Uses the recommended rules from @eslint-plugin-react
        "plugin:react-hooks/recommended",
        "plugin:jsx-a11y/strict",
    ],
    parserOptions: {
        ecmaFeatures: {
            jsx: true, // Allows for the parsing of JSX
        },
    },
    rules: {
        "react/prop-types": "off",
        "react-hooks/rules-of-hooks": "error",
        "react-hooks/exhaustive-deps": "error",
        "react/react-in-jsx-scope": "off",
        "react/display-name": "error",
        "react/require-optimization": "error",
    },
    settings: {
        react: {
            version: "detect", // Tells eslint-plugin-react to automatically detect the version of React to use
        },
    },
};